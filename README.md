# docker-php7
Docker build configuration, including apache vhost, docker-compose file

The following packages are includes:

- PHP7
- Apache 2.4
- composer
- docker-compose.yml file to link a **mysql container** and mount your database within your project folder

## Apache 2.4

The default apache vhost will point to /var/www/html/web. So you can mount your public folder there. An example for a 
docker-compose setup is included.

docker build -t smobiler/race_bot:v1 .

docker run --rm -e WEB_CONTAINER_NAME=race --name race -p 8080:80 -p 8002:8002 -p 8003:8003 -p 8004:8004 -p 8005:8005 -v /Users/smobiler/www/docker_racebotwebserver/build/custom-vhost.conf:/etc/apache2/sites-available/custom-vhost.conf -v /Users/smobiler/www/docker_racebotwebserver/build/php.ini:/usr/local/etc/php/conf.d/php.ini -v /Users/smobiler/www/racebotwebserver:/var/www/html smobiler/race_bot:v1 bash /var/www/html/start_server.sh

bash -c "clear && docker exec -it race bash"
